---
Spike Report
---

Graphics Spike 2
================

Introduction
------------

This Spike was completed in 2017 and the folder structure and repo was
updated in 2018.

Particle Systems can add details to a level that makes it feel much more
alive and prevents the level from feeling stale. However they can be
difficult initially to understand

Bitbucket Link: <https://bitbucket.org/bSalmon842/graphicsspike2>

Goals
-----

-   The Spike Report should answer each of the Gap questions

    -   Each particle system created should be marked as
        mobile-friendly, or not mobile-friendly based on the guidelines
        from [the mobile content creation
        guide](https://docs.unrealengine.com/latest/INT/Platforms/Mobile/Content/)\
        (don't forget to check that [the materials
        work](https://docs.unrealengine.com/latest/INT/Platforms/Mobile/Materials/index.html))

    -   Test using the [Mobile
        Previewer](https://docs.unrealengine.com/latest/INT/Platforms/Mobile/Previewer/index.html)

-   For each of the following non-technical particle briefs/requests,
    come up with a short plan, and then build the particle system.

    -   Rocket Launcher exhaust (the particle that comes out of a rocket
        launcher as you fire it)

    -   A snow or rain particle system which can be attached to the
        player (camera) to simulate weather effects.

    -   A red-to-yellow coloured mining laser beam, with "charging up"
        spikes at the start of the beam.

    -   A world war 1 fragmentation grenade effect using the Mesh type.

-   Use each of the following particle modules at least once. You can
    either include them in the above particles, or create additional
    particles to fulfil these requirements:

    -   Colour Modules

    -   Size modules

    -   Spawn Modules

    -   Velocity Modules

    -   Acceleration Modules

    -   Collision Modules

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://docs.unrealengine.com/latest/INT/Engine/Rendering/ParticleSystems/Overview/index.html>

Tasks undertaken
----------------

-   All systems in this Spike use 1 White Material, giving them a more
    simple blocky look

-   Rocket Launcher Exhaust uses 2 Emitters

    -   The first Emitter controls the exhaust flash, which appears for
        a very small amount of time

    -   The Second Emitter controls the smoke, which lingers a little
        longer and spreads out

-   Rain uses 1 emitter

    -   The emitter spreads out the drops around the player it would be
        attached to. If the emitter moves it will spawn more raindrops
        using the Spawn Per Unit module.

-   Mining Beam uses 5 emitters

    -   The first of the emitters controls the actual beam, which
        changes from Red to Yellow over Time

    -   The other 4 Emitters use different velocities to appear as
        though they are charge the central beam.

-   Grenade Explosion uses 3 emitters

    -   The first is the initial flash

    -   The second is the fragmentation which fly out in different
        directions

    -   The third is a small smoke cloud which disappears quickly

What we found out
-----------------

From this Spike we learned that the Cascade Particle Editor can be used
to quickly make multiple kinds of effects. We also learned how to turn
basic descriptions of particles into systems. Using simple shapes and
by adapting the guides in the Unreal Engine documentationsome small
tuning a particle effect is able to be created in approximately 30
minutes depending on the complexity.

Recommendations
---------------

It would be more beneficial for an artist to handle the particle systems
so that they can get the right effect, however it is still important for
a programmer to know how to use the particle systems so that they can
understand how they affect performance.